#!/bin/sh

mkdir -p build/META-INF
mkdir target
cp resources/js-policy/keycloak-scripts.json build/META-INF/
cp resources/js-policy/desk-owner-policy.js build/
cp resources/js-policy/generic-role-mapper.js build/
jar cvf target/ip-keycloak-custom-scripts.jar -C build .
