iparapheur Keycloak
===================

Wrapper around the official Keycloak Docker image :  
https://quay.io/keycloak/keycloak,  
with embedded iparapheur configuration files.

### Mandatory environment variables

```
- KC_DB_USERNAME
- KC_DB_PASSWORD
- KC_HOSTNAME
- KC_HOSTNAME_ADMIN
- KEYCLOAK_REALM
- KEYCLOAK_CLIENT_ID
- KEYCLOAK_WEB_CLIENT_ID
- KEYCLOAK_CLIENT_SECRET
- KEYCLOAK_ADMIN
- KEYCLOAK_ADMIN_PASSWORD
- INITIAL_IPARAPHEUR_ADMIN_USER
- INITIAL_IPARAPHEUR_ADMIN_PASSWORD
- KEYCLOAK_RESTADMIN_PASSWORD
- KEYCLOAK_SOAPUI_PASSWORD
- SMTP_MAIL_FROM
```

### External data needed as volumes

Login themes can be customized by mounting each of them to `/opt/keycloak/themes/`.
The current realm expects one theme named "iparapheur" (but will still works without).

### Standalone test

You can test the image in standalone mode, by building it locally (see contributing), creating a .env file (based on the .env.test example), and launching the service using the test compose file :

```bash
sudo docker compose -f docker-compose-test.yml up
```

You should see logs indicating the realm import
