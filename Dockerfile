#
# iparapheur Keycloak
# Copyright (C) 2022-2025 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
#

FROM hubquayio.libriciel.fr/keycloak/keycloak:25.0.6@sha256:82c5b7a110456dbd42b86ea572e728878549954cc8bd03cd65410d75328095d2 AS builder

# Env vars and files used for the build phase (settings that will be used in production mode *only*)

# Enable health and metrics support
ENV KC_HEALTH_ENABLED=true
ENV KC_METRICS_ENABLED=true

# Configure a database vendor
ENV KC_DB=postgres
ENV KC_PROXY=edge

ENV KC_FEATURES=scripts,token-exchange,admin-fine-grained-authz

ENV KC_HTTP_RELATIVE_PATH=/auth
ENV KC_HOSTNAME_PATH=/auth

COPY target/ip-keycloak-custom-scripts.jar /opt/keycloak/providers/
COPY target/keycloak-server-copy/providers/* /opt/keycloak/providers/
COPY resources/realm-bootstrap-19.0.3.json /opt/keycloak/data/import/

WORKDIR /opt/keycloak
RUN /opt/keycloak/bin/kc.sh build

# Everything down here will be accessible at run time (including in dev mode)

FROM hubquayio.libriciel.fr/keycloak/keycloak:25.0.6@sha256:82c5b7a110456dbd42b86ea572e728878549954cc8bd03cd65410d75328095d2

# Open Containers Initiative parameters
ARG CI_COMMIT_REF_NAME=""
ARG CI_COMMIT_SHA=""
ARG CI_PIPELINE_CREATED_AT=""
# Non-standard and/or deprecated variables, that are still widely used.
# If it is already set in the FROM image, it has to be overridden.
MAINTAINER Libriciel SCOP
LABEL maintainer="Libriciel SCOP"
LABEL org.label-schema.build-date="$CI_PIPELINE_CREATED_AT"
LABEL org.label-schema.name="ip-keycloak"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.vendor="Libriciel SCOP"
# Open Containers Initiative's image specifications
LABEL org.opencontainers.image.authors="Libriciel SCOP"
LABEL org.opencontainers.image.created="$CI_PIPELINE_CREATED_AT"
LABEL org.opencontainers.image.description="Official Keycloak image, with iparapheur parameters presets"
LABEL org.opencontainers.image.licenses="GNU Affero GPL v3"
LABEL org.opencontainers.image.revision="$CI_COMMIT_SHA"
LABEL org.opencontainers.image.source="registry.libriciel.fr:443/public/signature/ip-keycloak"
LABEL org.opencontainers.image.title="ip-keycloak"
LABEL org.opencontainers.image.vendor="Libriciel SCOP"
LABEL org.opencontainers.image.version="$CI_COMMIT_REF_NAME"

COPY ./resources/jq-1.7.1-amd64 /usr/bin/jq
COPY ./resources/curl-8.7.1-amd64 /usr/bin/curl

COPY --from=builder /opt/keycloak/ /opt/keycloak/

COPY scripts/* /opt/scripts/
COPY resources/themes/iparapheur /opt/keycloak/themes/iparapheur

ENV KC_DB=postgres
ENV KC_PROXY=edge
ENV KC_DB_URL=jdbc:postgresql://postgres:5432/

ENV KC_FEATURES=scripts,token-exchange,admin-fine-grained-authz

ENV KC_HTTP_RELATIVE_PATH=/auth
ENV KC_HOSTNAME_PATH=/auth

# The default JAVA_OPTS from the kc.sh file, without the Xms/Xmx parameters.
# This should be checked on every Keycloak update.
ENV JAVA_OPTS="-XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=256m -Dfile.encoding=UTF-8 -Dsun.stdout.encoding=UTF-8 -Dsun.err.encoding=UTF-8 -Dstdout.encoding=UTF-8 -Dstderr.encoding=UTF-8 -XX:+ExitOnOutOfMemoryError -Djava.security.egd=file:/dev/urandom -XX:+UseParallelGC -XX:MinHeapFreeRatio=10 -XX:MaxHeapFreeRatio=20 -XX:GCTimeRatio=4 -XX:AdaptiveSizePolicyWeight=90 -XX:FlightRecorderOptions=stackdepth=512"
# Our own tuning, with RAMPercentages in place or the Xms/Xms.
# See https://www.keycloak.org/server/containers
# TODO: use the JAVA_OPTS_KC_HEAP when available
ENV JAVA_OPTS_APPEND="-Djava.net.preferIPv4Stack=true -XX:InitialRAMPercentage=50 -XX:MaxRAMPercentage=70"

ENTRYPOINT ["/opt/scripts/docker-entrypoint.sh"]
# ENTRYPOINT ["/opt/keycloak/bin/kc.sh"]
