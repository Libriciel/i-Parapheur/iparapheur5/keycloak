<#--
  ~ iparapheur Keycloak
  ~ Copyright (C) 2022-2025 Libriciel-SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, version 3.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~
  ~ SPDX-License-Identifier: AGPL-3.0-only
  -->

<#import "template.ftl" as layout>

<#-- @ftlvariable name="url" type="java.lang.Object" -->
<#-- @ftlvariable name="username" type="java.lang.Object" -->

<@layout.registrationLayout displayInfo=true; section>


    <#-- The actual sections order is defined in the template.ftl file -->
    <#-- These are just blocks of injected code -->
    <#if section = "header">

        <p>${msg("updatePasswordTitle")}</p>

    <#-- The actual sections order is defined in the template.ftl file -->
    <#-- These are just blocks of injected code -->
    <#elseif section = "form">

        <form id="kc-passwd-update-form"
              action="${url.loginAction}"
              method="post">

            <#-- As of Keycloak 24, these invisible fields are actually used -->
            <#-- We have to keep those, with the ID fields untouched -->

            <#--noinspection HtmlFormInputWithoutLabel-->
            <input type="text"
                   id="username"
                   class="d-none"
                   name="username"
                   value="${username}"
                   autocomplete="username"
                   aria-hidden="true"
                   readonly="readonly"/>

            <#--noinspection HtmlFormInputWithoutLabel-->
            <input type="password"
                   id="password"
                   class="d-none"
                   name="password"
                   aria-hidden="true"
                   autocomplete="current-password"/>

            <#-- New password -->
            <div class="mt-3">
                <label for="password-new">
                    ${msg("passwordNew")}
                </label>
            </div>

            <div class="mt-2 input-group">

                <span class="input-group-text input-group-text-hint">
                    <i class="fa fa-lock"></i>
                </span>

                <input id="password-new"
                       name="password-new"
                       class="form-control border-end-0"
                       type="password"
                       autofocus
                       autocomplete="new-password"/>

                <span class="input-group-text input-group-text-visibility"
                      onclick="switchInputType('password-new', 'new-password-icon')">
                    <i id="new-password-icon"
                       class="fa fa-eye-slash">
                    </i>
                </span>

            </div>

            <#-- Confirm password -->
            <div class="mt-3">

                <label for="password-confirm"
                       class="w-100">
                    ${msg("passwordConfirm")}
                </label>

                <div class="mt-2 input-group">

                    <span class="input-group-text input-group-text-hint">
                        <i class="fa fa-lock"></i>
                    </span>

                    <input id="password-confirm"
                           name="password-confirm"
                           class="form-control border-end-0"
                           type="password"
                           autocomplete="off"/>

                    <span class="input-group-text input-group-text-visibility"
                          onclick="switchInputType('password-confirm', 'confirm-password-icon')">
                        <i id="confirm-password-icon"
                           class="fa fa-eye-slash">
                        </i>
                    </span>
                </div>

            </div>

            <div class="mt-4 d-flex flex-row-reverse">

                <#-- Send button -->
                <button class="btn btn-primary ms-2"
                        name="login">
                    <i class="fa fa-paper-plane fa-fw"></i>
                    ${msg("doSubmit")}
                </button>

            </div>

        </form>

    </#if>

</@layout.registrationLayout>
