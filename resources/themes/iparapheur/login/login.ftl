<#--
  ~ iparapheur Keycloak
  ~ Copyright (C) 2022-2025 Libriciel-SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, version 3.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~
  ~ SPDX-License-Identifier: AGPL-3.0-only
  -->

<#import "template.ftl" as layout>

<#-- @ftlvariable name="login" type="java.lang.Object" -->
<#-- @ftlvariable name="properties" type="java.lang.Object" -->
<#-- @ftlvariable name="realm" type="java.lang.Object" -->
<#-- @ftlvariable name="social" type="java.lang.Object" -->
<#-- @ftlvariable name="url" type="java.lang.Object" -->

<@layout.registrationLayout displayInfo=social.displayInfo displayWide=(realm.password && social.providers??); section>


    <#-- The actual sections order is defined in the template.ftl file -->
    <#-- These are just blocks of injected code -->
    <#if section = "socialProviders" >

        <#if realm.password && social?? && social.providers?has_content>

            <p class="w-100 text-center">
                ${msg("doProviderLogInInfo")}
            </p>

            <ul id="kc-social-providers"
                class="un-styled-list mt-3">
                <#list social.providers as provider>
                    <li>
                        <a id="social-${provider.alias}"
                           class="w-100 btn btn-primary provider-button mt-2"
                           href="${provider.loginUrl}">
                            ${msg("loginTitle", provider.displayName!)}
                        </a>
                    </li>
                </#list>
            </ul>

            <div class="w-100 d-inline-flex mt-4 mb-3">
                <hr class="flex-grow-1"/>
                <span class="mx-4 align-content-center">
                    OU
                </span>
                <hr class="flex-grow-1"/>
            </div>

        </#if>


    <#-- The actual sections order is defined in the template.ftl file -->
    <#-- These are just blocks of injected code -->
    <#elseif section = "header">

        <p class="w-100 text-center">
            <#if social?? && social.providers?has_content>
                ${msg("doLocalLogInInfo")}
            <#else>
                ${msg("doLogInInfo")}
            </#if>
        </p>


    <#-- The actual sections order is defined in the template.ftl file -->
    <#-- These are just blocks of injected code -->
    <#elseif section = "form">

        <form onsubmit="login.disabled = true; return true;"
              action="${url.loginAction}"
              method="post">

            <#-- Username -->
            <div class="mt-4 d-flex align-items-center">

                <label for="username"
                       class="input-label">
                    <#if !realm.loginWithEmailAllowed>
                        ${msg("username")}
                    <#elseif !realm.registrationEmailAsUsername>
                        ${msg("usernameOrEmail")}
                    <#else>
                        ${msg("email")}
                    </#if>
                </label>

                <div class="input-group">

                    <span class="input-group-text input-group-text-hint">
                        <i class="fa fa-user"></i>
                    </span>

                    <#if usernameEditDisabled??>
                        <input id="username"
                               class="form-control"
                               name="username"
                               value="${(login.username!'')}"
                               type="text"
                               disabled/>
                    <#else>
                        <input id="username"
                               class="form-control"
                               name="username"
                               value="${(login.username!'')}"
                               type="text"
                               autofocus
                               autocomplete="off"/>
                    </#if>
                </div>
            </div>

            <#-- Password -->
            <div class="mt-3 d-flex align-items-center">

                <label for="password"
                       class="input-label">
                    ${msg("password")}
                </label>

                <div class="flex-grow-1 input-group">
                    <span class="input-group-text input-group-text-hint">
                        <i class="fa fa-lock"></i>
                    </span>
                    <input class="form-control border-end-0"
                           id="password"
                           name="password"
                           type="password"
                           autocomplete="off"/>
                    <span class="input-group-text input-group-text-visibility"
                          onclick="switchInputType('password', 'password-icon')">
                        <i id="password-icon"
                           class="fa fa-eye-slash">
                        </i>
                    </span>
                </div>

            </div>

            <div class="mt-4 d-flex flex-row-reverse">

                <#-- Login button -->
                <button class="btn btn-primary ms-2"
                        name="login">
                    <i class="fa fa-sign-in fa-fw"></i>
                    ${msg("doLogIn")}
                </button>

                <#-- Reset password -->
                <#if realm.resetPasswordAllowed>
                    <a class="btn btn-light"
                       href="${url.loginResetCredentialsUrl}">
                        ${msg("doForgotPassword")}
                    </a>
                </#if>

            </div>
        </form>


    <#-- The actual sections order is defined in the template.ftl file -->
    <#-- These are just blocks of injected code -->
    <#elseif section = "info" >

        <#if realm.password && realm.registrationAllowed && !usernameEditDisabled??>
            <div id="kc-registration">
                <span>${msg("noAccount")}
                    <a href="${url.registrationUrl}">
                        ${msg("doRegister")}
                    </a>
                </span>
            </div>
        </#if>

    </#if>

</@layout.registrationLayout>
