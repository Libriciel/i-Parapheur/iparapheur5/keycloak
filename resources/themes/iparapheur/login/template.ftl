<#--
  ~ iparapheur Keycloak
  ~ Copyright (C) 2022-2025 Libriciel-SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, version 3.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~
  ~ SPDX-License-Identifier: AGPL-3.0-only
  -->

<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayWide=false displayRequiredFields=false>

    <!doctype html>
    <html class="${properties.kcHtmlClass!}"
          lang="fr-FR">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta name="robots" content="noindex, nofollow">

            <#if properties.meta?has_content>
                <#list properties.meta?split(' ') as meta>
                    <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
                </#list>
            </#if>
            <title>${msg("loginTitle",(realm.displayName!''))}</title>
            <link rel="icon" href="${url.resourcesPath}/img/favicon.ico"/>
            <link rel="stylesheet"
                  href="https://fonts.googleapis.com/css?family=Ubuntu">
            <#if properties.styles?has_content>
                <#list properties.styles?split(' ') as style>
                    <link href="${url.resourcesPath}/${style}" rel="stylesheet"/>
                </#list>
            </#if>
            <#if properties.scripts?has_content>
                <#list properties.scripts?split(' ') as script>
                    <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
                </#list>
            </#if>
            <#if scripts??>
                <#list scripts as script>
                    <script src="${script}" type="text/javascript"></script>
                </#list>
            </#if>
            <script src="${url.resourcesPath}/js/login.js" type="text/javascript"></script>
            <script src="${url.resourcesPath}/js/bootstrap.min.js" type="text/javascript"></script>
        </head>

        <body class="vh-100 d-flex flex-column">

            <main class="flex-grow-1 d-flex flex-row">

                <div class="left-panel bg-light d-flex justify-content-center align-items-center">
                    <div class="p-4">
                        <img src="${url.resourcesPath}/images/logo-ls-full.svg" alt="Libriciel-SCOP"/>
                        <div class="w-100 mt-3 d-inline-flex justify-content-center align-middle">
                            ${msg("tagLine")}
                        </div>
                    </div>
                </div>

                <div class="flex-grow-1 d-flex justify-content-center align-items-center">
                    <div class="main-content">
                        <div class="mb-4">
                            <img src="${url.resourcesPath}/images/logo-ip-full.svg"
                                 alt="iparapheur"/>
                        </div>

                        <div>

                            <#nested "socialProviders">

                            <#nested "header">

                            <#if displayMessage && message?has_content && (message.type = "error")>
                                <div class="mt-3 alert alert-danger"
                                     role="alert">
                                    ${kcSanitize(message.summary)?no_esc}
                                </div>
                            <#elseif displayMessage && message?has_content>
                                <div class="alert alert-info"
                                     role="alert">
                                    ${kcSanitize(message.summary)?no_esc}
                                </div>
                            </#if>

                            <#nested "form">

                            <#if displayInfo>
                                <div id="kc-info"
                                     class="${properties.kcSignUpClass!}">
                                    <div id="kc-info-wrapper"
                                         class="${properties.kcInfoAreaWrapperClass!}">
                                        <#nested "info">
                                    </div>
                                </div>
                            </#if>
                        </div>

                    </div>
                </div>
            </main>

            <div class="bg-dark footer d-flex align-items-center px-4">
                <div class="flex-grow-1 flex-basis-zero">
                    <#list [
                            ["asalae", "logo-as.svg", "asalae"],
                            ["comelus", "logo-ce.svg", "comélus"],
                            ["idelibre", "logo-id.svg", "idelibre"],
                            ["iparapheur", "logo-ip.svg", "iparapheur"],
                            ["pastell", "logo-pa.svg", "pastell"],
                            ["refae", "logo-re.svg", "refae"],
                            ["slow", "logo-sl.svg", "s²low"],
                            ["versae", "logo-ve.svg", "versae"],
                            ["webdelib", "logo-wd.svg", "webdelib"],
                            ["webgfc", "logo-wg.svg", "webgfc"]
                        ] as software>
                        <a href="https://www.libriciel.fr/logiciels/${software[0]}"
                           class="me-2"
                           target="_blank">
                            <img src="${url.resourcesPath}/images/${software[1]}"
                                 class="footer-icon"
                                 alt="En savoir plus sur l'application ${software[2]}"/></a>
                    </#list>
                </div>

                <div class="text-white opacity-40">
                    iparapheur &copy; Libriciel SCOP 2006-2025
                </div>

                <div class="flex-grow-1 flex-basis-zero text-end">
                    <a href="https://www.libriciel.fr"
                       target="_blank">
                        <img src="${url.resourcesPath}/images/logo-ls.svg"
                             class="footer-icon"
                             alt="Accéder au site de Libriciel-SCOP"/></a>
                </div>
            </div>

        </body>
    </html>
</#macro>
