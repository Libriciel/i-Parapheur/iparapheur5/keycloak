<#--
  ~ iparapheur Keycloak
  ~ Copyright (C) 2022-2025 Libriciel-SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, version 3.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~
  ~ SPDX-License-Identifier: AGPL-3.0-only
  -->

<#import "template.ftl" as layout>

<@layout.registrationLayout displayMessage=false; section>


    <#-- The actual sections order is defined in the template.ftl file -->
    <#-- These are just blocks of injected code -->
    <#if section = "header">

        <#if messageHeader??>
            <p class="alert alert-info">
                ${kcSanitize(msg("${messageHeader}"))?no_esc}
            </p>
        <#else>
            <p class="alert alert-info">
                ${message.summary}
            </p>
        </#if>


    <#-- The actual sections order is defined in the template.ftl file -->
    <#-- These are just blocks of injected code -->
    <#elseif section = "form">

        <div id="kc-info-message">
            <p class="instruction">${message.summary}
                <#if requiredActions??>
                    <#list requiredActions>:
                        <b>
                            <#items as reqActionItem>
                                ${kcSanitize(msg("requiredAction.${reqActionItem}"))?no_esc}<#sep>,
                            </#items>
                        </b>
                    </#list>
                <#else>
                </#if>
            </p>
            <#if skipLink??>
            <#else>
                <#if pageRedirectUri?has_content>
                    <p>
                        <a class="btn btn-light"
                           href="${pageRedirectUri}">
                            <i class="fa fa-arrow-left"></i>
                            ${kcSanitize(msg("backToApplication"))?no_esc}
                        </a>
                    </p>
                <#elseif actionUri?has_content>
                    <p>
                        <a href="${actionUri}">
                            ${kcSanitize(msg("proceedWithAction"))?no_esc}
                        </a>
                    </p>
                <#elseif (client.baseUrl)?has_content>
                    <p>
                        <a class="btn btn-light"
                           href="${client.baseUrl}">
                            <i class="fa fa-arrow-left"></i>
                            ${kcSanitize(msg("backToApplication"))?no_esc}
                        </a>
                    </p>
                </#if>
            </#if>
        </div>

    </#if>

</@layout.registrationLayout>
