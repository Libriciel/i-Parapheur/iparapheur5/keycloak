<#--
  ~ iparapheur Keycloak
  ~ Copyright (C) 2022-2025 Libriciel-SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, version 3.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~
  ~ SPDX-License-Identifier: AGPL-3.0-only
  -->

<#import "template.ftl" as layout>

<@layout.registrationLayout displayMessage=false; section>

    <#-- The actual sections order is defined in the template.ftl file -->
    <#-- These are just blocks of injected code -->
    <#if section = "header">

        <p>${msg("errorTitle")}</p>

    <#-- The actual sections order is defined in the template.ftl file -->
    <#-- These are just blocks of injected code -->
    <#elseif section = "form">

        <p class="alert alert-danger">${message.summary}</p>
        <#if client?? && client.baseUrl?has_content>
            <a class="btn btn-light"
               href="${client.baseUrl}">
                <i class="fa fa-arrow-left"></i>
                ${kcSanitize(msg("backToApplication"))?no_esc}
            </a>
        </#if>

    </#if>

</@layout.registrationLayout>
