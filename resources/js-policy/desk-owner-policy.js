var identity = $evaluation.getContext().getIdentity();
var resource = $evaluation.getPermission().getResource();
// Straightforward test, since resource and realm role share the same name
if (identity.hasRealmRole(resource.getName())) {
    $evaluation.grant();
}
