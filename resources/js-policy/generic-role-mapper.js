/**
 * Available variables:
 * user - the current user
 * realm - the current realm
 * token - the current token
 * userSession - the current userSession
 * keycloakSession - the current keycloakSession
 */

// Not proud of this one,
// but we don't have any parameter but a request header
// To pass an argument in here
var targetRoleName = keycloakSession
    .getContext()
    .getRequestHeaders()
    .getHeaderString("X-Scope-Mapper-Filtering");

// TODO : Find a proper hasRole()
var roles = [];
if (targetRoleName !== null){
  var targetRoleModel = realm.getRole(targetRoleName);

  if (targetRoleModel !== null && user.hasRole(targetRoleModel)) {
    roles.push(targetRoleModel.getName());
  }
}

roles.push("default-roles-api");
roles.push("offline_access");
roles.push("uma_authorization");

// Overriding the realm_access part
token.setOtherClaims("realm_access", {
    "roles": Java.to(roles, "java.lang.String[]"),
});
