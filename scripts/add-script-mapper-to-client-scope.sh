#!/bin/bash

echo "Start of add-script-mapper-to-client-scope.sh";

# Connect with the rest-admin account, on the realm 'api'
/opt/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080/auth --realm ${KEYCLOAK_REALM} --user rest-admin --password ${KEYCLOAK_RESTADMIN_PASSWORD}

# Retrieve the KC 'client' named 'ipcore-api' (returns a list, it's a search), and parse it's id
/opt/keycloak/bin/kcadm.sh get clients?clientId=${KEYCLOAK_CLIENT_ID} > /tmp/ipcore-api-cli.json
export CLIENT_ID=`cat /tmp/ipcore-api-cli.json |jq -r '.[0].id'`

# Retrieve the list of protocolMappers active on this client, and check if one is the 'generic_role_mapper' (store true or false in the var)
/opt/keycloak/bin/kcadm.sh get clients/$CLIENT_ID/evaluate-scopes/protocol-mappers > /tmp/protocol-mappers.json
export SCRIPT_MAPPER_EXISTS=`cat /tmp/protocol-mappers.json | jq 'any(.mapperName == "generic_role_mapper")'`


if [ ${SCRIPT_MAPPER_EXISTS} = "false" ];
then
  echo "The script mapper is not active, must create it";
  /opt/keycloak/bin/kcadm.sh create clients/$CLIENT_ID/protocol-mappers/models -s protocol=openid-connect -s protocolMapper=script-generic-role-mapper.js -s name=generic_role_mapper -s 'config."claim.name"=""' -s config.multivalued=false -s 'config."jsonType.label"=""'  -s 'config."id.token.claim"="true"'  -s 'config."access.token.claim"="true"' -s 'config."userinfo.token.claim"="true"'

else
  echo "The generic script mapper already exists";
fi

rm /tmp/ipcore-api-cli.json /tmp/protocol-mappers.json
