#!/bin/bash

echo "running ip-keycloak entrypoint script, passed vars : $@"

function configure_keycloak(){
while :
do
  echo "Waiting for KC to be up to execute IP post-install script"
  if curl -s http://localhost:8080/auth; then
    echo "Executing IP post-install script"
    /bin/bash -c "/opt/scripts/add-script-mapper-to-client-scope.sh"
    /bin/bash -c "/opt/scripts/secure-master-realm.sh"
    break;
  fi
  sleep 5
done
}

#configure_keycloak &> /dev/null & disown
configure_keycloak &
echo “Launched background process to configure Keycloak”


/bin/bash -c "/opt/keycloak/bin/kc.sh $*"
