#!/bin/bash

echo "Start of secure-master-realm.sh";

# Connect with the master admpin account, on the realm 'master'
/opt/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user ${KEYCLOAK_ADMIN} --password ${KEYCLOAK_ADMIN_PASSWORD}

# retrieve the realm settings as a json
/opt/keycloak/bin/kcadm.sh get / > /tmp/master-realm.json

# update the targeted values in the json
cat /tmp/master-realm.json | jq '.bruteForceProtected |= true | .failureFactor |= 3' > /tmp/updated-master-realm.json

# upadte the realm with it's the new settings
/opt/keycloak/bin/kcadm.sh update realms/master -f /tmp/updated-master-realm.json

# cleaup juste in case
rm /tmp/updated-master-realm.json /tmp/master-realm.json

echo "End of secure-master-realm.sh";
