Contributing
============

There are two steps to build the iparapheur-keycloak image.  

The first is to build the custom scripts jar file, that is used to provide a custom JS policy and a custom JS mapper to Keycloak.
The second is to build the docker image itself, providing said jar, as well as the realm's bootstrap JSON, and the docker entrypoint script.


### Build the jar

There is a simple script that builds automatically the jar and places it in the `target` directory (automatically overwriting the previous version in this directory) :

```bash
./build-custom-scripts-jar.sh
```  

The script relies on the availability of the `jar` command, obviously.

In short, what it does is creating a `build` directory, copying the necessary resources from `resources/js-policy/` (some js files and a json manifest) in the correct layout, and packaging that directory into a jar.


### Building the image
Classic :
```bash
sudo docker build --pull -t <target-image-name:target-image-tag> .
```

In order to use the test compose file, you should build a local version of the image named `ip-keycloak:dev` :

```bash
sudo docker build --pull -t ip-keycloak:dev .
```


### Login page Customization

On most cases, an SSO might be a most appropriate answer to this.  
A separated login logic is often a nice-to-have, and solve this point already.

The Libriciel support ends here.

#### Create a dedicated Keycloak project

To customize the login page, we first have to download the appropriate Keycloak tag on GitHub, and retrieve a `base` theme:
https://github.com/keycloak/keycloak/tree/23.0.7/themes/src/main/resources/theme  

Create a new code projet.

Add into it the Keycloak base theme, and change it at will.
The login sub-folder should be the only interesting customizing material.  
The Keycloak Admin Console shall not be used, therefore not be customized.  

Then apply the following Dockerfile:

```Dockerfile
FROM registry.libriciel.fr:443/public/signature/ip-keycloak:23.0.7.0@sha256:78930f391fa706a47fe13ca29b837a794b22f5348fca613b3524d98e6282fd28

ARG IPARAPHEUR_THEME_PATH="/opt/keycloak/themes/iparapheur"
RUN rm -rf $IPARAPHEUR_THEME_PATH
COPY /opt/iparapheur/current/custom-theme $IPARAPHEUR_THEME_PATH
```

#### Project maintenance

The customized image shall be rebuilt on every iparapheur patch, following the iparapheur Keycloak's versions.

Keycloak themes and template engine may change over time. It already did, once, in the last 10 major versions.  
The templating evolution is Keycloak's own and only disclosure.

Therefore, the customized Keycloak shall be re-build and tested on every update.

#### Override the existing Keycloak image

and create (if it does not exist yet), the `/opt/iparapheur/current/docker-compose.override.yml` file, containing :

```yaml
services:
  keycloak:
    image: path/to/my/registry/custom-image
```

